package INF102.lab5.graph;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class AdjacencySet<V> implements IGraph<V> {

    private Set<V> nodes;
    public Map<V, Set<V>> nodeToNode;

    public AdjacencySet() {
        nodes = new HashSet<>();
        nodeToNode = new HashMap<>();
    }

    @Override
    public int size() {
        return nodes.size();
    }

    @Override
    public Set<V> getNodes() {
        return Collections.unmodifiableSet(nodes);
    }

    @Override
    public void addNode(V node) {
        if(!nodes.contains(node)){
            nodes.add(node);
            nodeToNode.put(node, new HashSet<V>());
        }else{

        }
        /* 
        if(hasNode(node)) return;
        nodes.add(node);
        if(!nodes.contains(node)){
            nodes.add(node);
            nodeToNode.put(node, new HashSet<V>());
        }
        */
    }
        
    @Override
    public void removeNode(V node) {         
        if (nodes.contains(node)) {
            nodes.remove(node);
            nodeToNode.remove(node);

            for (V neighbour : nodeToNode.keySet()) {
                nodeToNode.get(neighbour).remove(node);
                }
            }
            
        }

    @Override
    public void addEdge(V u, V v) {
        if(!hasNode(u) || !hasNode(v))
        throw new IllegalArgumentException("You can't add an edge to a node that doesn't exist on the graph.");
        if (!hasEdge(u, v)) {
            nodeToNode.get(u).add(v);
            nodeToNode.get(v).add(u);
        }   
    }

    @Override
    public void removeEdge(V u, V v) {

        nodeToNode.get(u).remove(v);
        nodeToNode.get(v).remove(u);
    }

    @Override
    public boolean hasNode(V node) {
        return nodeToNode.containsKey(node);
    }

    @Override
    public boolean hasEdge(V u, V v) {
        return nodeToNode.get(u).contains(v);
    }

    @Override
    public Set<V> getNeighbourhood(V node) {
        return Collections.unmodifiableSet(nodeToNode.get(node));
    }

    @Override
    public String toString() {
        StringBuilder build = new StringBuilder();
        for (V node : nodeToNode.keySet()) {
            Set<V> nodeList = nodeToNode.get(node);

            build.append(node);
            build.append(" --> ");
            build.append(nodeList);
            build.append("\n");
        }
        return build.toString();
    }

}
