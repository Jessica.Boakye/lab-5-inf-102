package INF102.lab5.graph;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;

/**
 * This class is used to conduct search algorithms of a graph
 */
public class GraphSearch<V> implements IGraphSearch<V> {

    private IGraph<V> graph;

    public GraphSearch(IGraph<V> graph) {
        this.graph = graph;
    }

    @Override
    public boolean connected(V u, V v) {
    if (!graph.hasNode(u) || !graph.hasNode(v)) {
        return false; // En av nodene eksisterer ikke i grafen
    }   
    Queue<V> queue = new LinkedList<>();
    Set<V> visited = new HashSet<>();

    queue.add(u);
    visited.add(u);
        
    while (!queue.isEmpty()) {
        V currentNode = queue.poll();
        
        // Sjekk om vi har funnet noden v
        if (currentNode.equals(v)) {
            return true;
        }
        
        for (V neighbor : graph.getNeighbourhood(currentNode)) { // Use set.getNeighbourhood(currentNode) to get the neighbors.
            if (!visited.contains(neighbor)) {
                queue.add(neighbor);
                visited.add(neighbor);
            }
        }
    }
    return false;
}
        
}
